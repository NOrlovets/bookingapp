import java.util.LinkedList;
import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QueueHandler {
    private Queue<HotelBooking> queue = new LinkedList<HotelBooking>();
    private final int limit;
    BookingApp bookingApp = new BookingApp();
    int value = bookingApp.getValue();
    Logger logger = LoggerFactory.getLogger(QueueHandler.class);


    public QueueHandler(int limit) {
        this.limit = limit;
    }


    public synchronized void enqueue(HotelBooking t)
            throws InterruptedException {
        while (this.queue.size() == this.limit) {
            wait();
        }
        //if (this.queue.size() == 0) {

        //}
        logger.info("PRODUCER " + Thread.currentThread().getName() + " SENT " + t.getDate() + " " + t.getName());
        this.queue.add(t);
        notifyAll();
        bookingApp.setValue(value -= 1);
    }


    public synchronized Object dequeue()
            throws InterruptedException {

        while (this.queue.size() == 0 && value > 0) {
            wait();
        }
        if (value!=0){
            logger.info(this.queue.size() + " SL " + this.limit);
        //if (this.queue.size() == this.limit) {

        //}
        HotelBooking t = this.queue.poll();
        logger.info("Consumer " + Thread.currentThread().getName() + " PROCESSED " + t.getDate() + " " + t.getName());
        notifyAll();
        return this.queue.remove(0);
        }
        return 0;
    }
}
