
public class BookingApp {
    public static int value = 15;

    public synchronized static int getValue() {
        return value;
    }

    public synchronized static void setValue(int value) {
        BookingApp.value = value;
    }

    public static void main(String[] args) throws InterruptedException {

        QueueHandler queueHandler = new QueueHandler(5);
        Producer p1 = new Producer(queueHandler);
        Producer p2 = new Producer(queueHandler);
        Producer p3 = new Producer(queueHandler);

        Consumer c1 = new Consumer(queueHandler);
        Consumer c2 = new Consumer(queueHandler);
        Consumer c3 = new Consumer(queueHandler);
        Consumer c4 = new Consumer(queueHandler);
        Consumer c5 = new Consumer(queueHandler);
        Consumer c6 = new Consumer(queueHandler);

        p1.start();
        p2.start();
        p3.start();

        c1.start();
        c2.start();
        c3.start();
        c4.start();
        c5.start();
        c6.start();

        //c1.join();
    }
}
