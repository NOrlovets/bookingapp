import java.util.GregorianCalendar;

public class Producer extends Thread {

    private QueueHandler queueHandler;

    Producer(QueueHandler queueHandler) {
        this.queueHandler = queueHandler;
    }

    @Override
    public void run() {
        BookingApp bookingApp = new BookingApp();
        int value = bookingApp.getValue();
        while (value >= 0) {

            value = bookingApp.getValue();
            if (value != 0) {
                try {
                    GregorianCalendar gc = new GregorianCalendar();
                    int year = 2000 + (int) Math.round(Math.random() * (2017 - 2000));
                    gc.set(gc.YEAR, year);
                    int dayOfYear = 1 + (int) Math.round(Math.random() * (30 - 1));
                    gc.set(gc.DAY_OF_YEAR, dayOfYear);
                    String s1 = Integer.toString(gc.get(gc.YEAR));
                    String s2 = Integer.toString(gc.get(gc.MONTH));
                    String s3 = Integer.toString(gc.get(gc.DAY_OF_MONTH));
                    String date = s1 + "-" + s2 + "-" + s3;

                    int randomNum = 1 + (int) (Math.random() * ((30 - 1) + 1));
                    String name = Integer.toString(randomNum);
                    queueHandler.enqueue(new HotelBooking(date, name));
                    Thread.sleep(1000);
                    value = bookingApp.getValue();
                } catch (InterruptedException exception) {
                }
            }
            else {
                System.out.println("11111111111111111 "+Thread.currentThread().getName());
                break;
            }
        }
        System.out.println("END");
    }


}
